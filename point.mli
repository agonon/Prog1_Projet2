type point
val get_x : point -> float
val get_y : point -> float
val create : float -> float -> point
