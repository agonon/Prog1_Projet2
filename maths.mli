type matrix

val set_coeff : float -> matrix -> int -> int -> unit
val length : matrix -> int
val get_coeff : matrix -> int -> int -> float
val create_c_by_c : float list -> int -> matrix
val determinant : matrix -> float
