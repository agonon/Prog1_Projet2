type triangle_set
val empty_ts : unit -> triangle_set
val is_empty_ts : triangle_set -> bool
val cons_ts : Triangle.triangle -> triangle_set -> triangle_set
val car_ts : triangle_set -> Triangle.triangle
val cdr_ts : triangle_set -> triangle_set
val length_ts : triangle_set -> int
val concat_ts : triangle_set -> triangle_set -> triangle_set
