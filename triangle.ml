open Maths;;
open Point;;

type triangle = {p1: point; p2: point; p3: point};;

let get_p1 t = t.p1;;
let get_p2 t = t.p2;;
let get_p3 t = t.p3;;

let create_triangle point1 point2 point3 =
  {p1= point1; p2= point2; p3= point3};;

let ccw a b c =
  let l = [get_x b -. get_x a; get_y b -. get_y a; get_x c -. get_x a; get_y c -. get_y a] in
  let m = create_c_by_c l 2 in
  if determinant m > 0. then true
  else false;;
