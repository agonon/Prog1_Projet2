#!/bin/bash

ocamlc -c point.mli
ocamlc -c point.ml

ocamlc -c point_set.mli
ocamlc -c point_set.ml

ocamlc -c maths.mli
ocamlc -c maths.ml

ocamlc -c triangle.mli
ocamlc -c triangle.ml

ocamlc -c triangle_set.mli
ocamlc -c triangle_set.ml

ocamlc -c drawing.mli
ocamlc -c drawing.ml

ocamlc -c delaunay.mli
ocamlc -c delaunay.ml

echo "La compilation est terminée."
